# PieFed

A Lemmy/Mbin alternative written in Python with Flask.

 - Clean, simple code that is easy to understand and contribute to. No fancy design patterns or algorithms.
 - Easy setup, easy to manage - few dependencies and extra software required.
 - AGPL.
 - First class moderation tools.

## Project goals

To build a federated discussion and link aggregation platform, similar to Reddit, Lemmy, Mbin interoperable with as
much of the fediverse as possible.

## For developers

- [Screencast: overview of the PieFed codebase](https://join.piefed.social/2024/01/22/an-introduction-to-the-piefed-codebase/)
- [Database / entity relationship diagram](https://join.piefed.social/wp-content/uploads/2024/02/PieFed-entity-relationships.png)
- see [INSTALL.md](INSTALL.md)
- see docs/project_management/* for a project roadmap, contributing guide and much more.
